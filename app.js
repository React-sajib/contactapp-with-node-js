const express = require('express')
const morgan = require('morgan')
const App = express()

App.use(morgan('dev'))
App.use(express.urlencoded({extended:true}))
App.use(express.json())



//Routes
const ContactRoutes = require('./src/Components/Routes/ContactRoutes')
App.use('/contacts',ContactRoutes)



//if any error in Routes
App.get('*',(req,res)=>{
    res.send('<h1>404 No Page Found</h1>')
})

//server running on port
const PORT = process.env.PORT || 8080 

App.listen( PORT,()=>{
    console.log(`sarver is running on port ${PORT}`)
})