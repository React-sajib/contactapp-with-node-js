class Contacts{
    constructor(){
        this.contacts = []
    }

    //get all contacts
    GetallContacts = ()=>{
        return this.contacts
    }

    //Create new contact
    CreateContact = (Contact)=>{
        Contact.id = this.contacts.length + 1
        this.contacts.push(Contact)
        return Contact
    }

    //get contacts by id
    GetContactsById = (id)=>{
       return this.contacts.find((data,i)=>data.id == id)
    }


    //update contact by id
    UpdateContactById = ( id,UpdatedContact)=>{
        let index = this.contacts.findIndex((data,i)=>data.id == id)
        this.contacts[index].name = UpdatedContact.name || this.contacts[index].name
        this.contacts[index].phone = UpdatedContact.phone || this.contacts[index].phone
        this.contacts[index].email = UpdatedContact.email || this.contacts[index].email

        return this.contacts[index]
    } 


    DeleteContactById = (id)=>{
        let index = this.contacts.findIndex((data,i)=>data.id == id)
        let delObj = this.contacts[index]
        this.contacts = this.contacts.filter(cont => cont.id !== id ) 

        return delObj
    }


}



module.exports = new Contacts() 