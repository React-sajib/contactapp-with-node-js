const Contacts = require('../Model/ContactsModel')


//show all contact
exports.getAllcontacts = (req,res)=>{

    res.json(Contacts.GetallContacts())
    
}

//create contact
exports.createContacts = (req,res)=>{
    let { name,email,phone } = req.body
    let Newcontact =  Contacts.CreateContact({
        name,
        email,
        phone,
    })
    // console.log(Newcontact)
    res.json(Newcontact)
}

//get contact by id
exports.getsingleContact = (req,res)=>{
    let { id } = req.params

    id = parseInt(id)
    let contact =   Contacts.GetContactsById(id)
    res.json(contact)

}

//update contact
exports.updatecontact = (req,res)=>{

    let { id } = req.params

    id = parseInt(id)

    let { name,email,phone } = req.body
    
    let updateContact = Contacts.UpdateContactById(id,{
        name,
        email,
        phone
    })

    res.json(updateContact)




}

//delete contact
exports.deleteContact = (req,res)=>{
    let { id } = req.params

    id = parseInt(id)

    let DeleteContact =   Contacts.DeleteContactById(id)

    res.json(DeleteContact)
}