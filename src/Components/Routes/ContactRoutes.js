const router = require('express').Router()

const { 
    getAllcontacts,
    createContacts,
    getsingleContact,
    updatecontact,
    deleteContact,

 } = require('./../Controllers/ContactController')

    router.get('/',getAllcontacts)
    router.post('/',createContacts)
    router.get('/:id',getsingleContact)
    router.put('/:id',updatecontact)
    router.delete('/:id',deleteContact)


module.exports = router